import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class EmailTest {

    //name.lastname@sda.com

    @Test
    void shouldCreateValidEmail(){
        //given
        String name = "Lewis";
        String lastName = "Hamilton";

        //when
        String email = Email.createEmail(name, lastName);

        //then
        assertThat(email)
                .isNotNull()
                .contains(".")
                .startsWith("lewis")
                .endsWith("@sda.com")
                .contains("hamilton")
                .isLowerCase();
    }
}
